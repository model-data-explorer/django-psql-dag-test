from django.apps import AppConfig


class DagTestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "dag_test"
