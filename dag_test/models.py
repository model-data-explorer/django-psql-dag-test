from django.db import models
from django_postgresql_dag.models import node_factory, edge_factory
from django.contrib.auth.models import User, Group


class DataGroupEdge(edge_factory("dag_test.DataGroupNode", concrete=False)):

    def __str__(self):
        return (
            f"{self.parent.tree_type}: {self.parent.datagroup} "
            "→ {self.child.datagroup}"
        )


class DataGroupNode(node_factory(DataGroupEdge)):
    """A node to display relations between data groups."""

    class Meta:

        unique_together = ("tree_type", "datagroup")

    class TreeType(models.TextChoices):
        """Type of the tree for a datagroup."""

        parent_tree = "PARENT", "Parent-Child tree"
        member_tree = "MEMBER", "Member inheritance tree"
        list_tree = "LIST", "Dataset listing tree"

    tree_type = models.CharField(max_length=10, choices=TreeType.choices)
    datagroup = models.ForeignKey("DataGroup", on_delete=models.CASCADE)


class DataGroupUserGroup(Group):
    """A group with a certain role in a :class:`DataGroup`"""

    class Roles(models.TextChoices):
        owner = "OWNER", "owner priviliges"
        data_manager = "DATAMANAGER", "data manager privileges"
        data_editor = "DATAEDITOR", "data editor privileges"
        editor = "EDITOR", "editor privileges"
        member = "MEMBER", "view permissions"

    datagroup = models.ForeignKey("DataGroup", on_delete=models.CASCADE)
    role = models.CharField(max_length=20, choices=Roles.choices)


class DataGroupRelation(models.Model):
    """An relation between two datagroups that awaits approval."""

    class Meta:

        unique_together = ("child_group", "parent_group", "tree_type")

    child_group = models.ForeignKey(
        "DataGroup", on_delete=models.CASCADE, related_name="%(class)s_child"
    )
    parent_group = models.ForeignKey(
        "DataGroup", on_delete=models.CASCADE, related_name="%(class)s_parent"
    )

    child_approved = models.BooleanField(default=False)
    parent_approved = models.BooleanField(default=False)

    child_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(class)s_child_approval",
    )
    parent_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(class)s_parent_approval",
    )

    tree_type = models.CharField(
        max_length=10, choices=DataGroupNode.TreeType.choices
    )


class DataGroup(models.Model):
    """A DataGroup in the Model Data Explorer"""

    class Meta:

        app_label = "dag_test"

    name = models.CharField(max_length=100)

    tags = models.ManyToManyField("Tag")

    def __str__(self):
        return self.name


class DataGroupUserRelation(models.Model):
    """A direct relation between a user and a data group."""

    class Meta:
        unique_together = ("datagroup", "user", "role")

    datagroup = models.ForeignKey(DataGroup, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    role = models.CharField(
        max_length=20, choices=DataGroupUserGroup.Roles.choices
    )

    datagroup_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="datagroupuserrelation_datagroup_approval",
    )
    user_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="datagroupuserrelation_user_approval",
    )


class Dataset(models.Model):
    """A dataset in the model data explorer."""

    name = models.CharField(max_length=100)

    tags = models.ManyToManyField("Tag")


class DatasetRelationBase(models.Model):
    """Abstract base for a relation between dataset and user or group."""

    class Meta:

        unique_together = ("dataset", "related_party", "role")
        abstract = True

    class Roles(models.TextChoices):
        owner = "OWNER", "owner priviliges"
        data_manager = "DATAMANAGER", "data manager privileges"
        data_editor = "DATAEDITOR", "data editor privileges"
        editor = "EDITOR", "editor privileges"
        member = "MEMBER", "view permissions"


    dataset = models.ForeignKey(
        "Dataset", on_delete=models.CASCADE, related_name="%(class)s"
    )
    role = models.CharField(
        max_length=20, choices=Roles.choices
    )

    dataset_approved = models.BooleanField(default=False)
    related_party_approved = models.BooleanField(default=False)

    dataset_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(class)s_dataset_approval",
    )
    related_party_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(class)s_related_party_approval",
    )


class DatasetDataGroupRelation(DatasetRelationBase):
    """A permission for a relation."""

    related_party = models.ForeignKey(DataGroup, on_delete=models.CASCADE)


class DatasetUserRelation(DatasetRelationBase):
    """A permission for a relation."""

    related_party = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="%(class)s_related_party"
    )


class TagEdge(edge_factory("dag_test.Tag", concrete=False)):
    """An edge for a DataGroup or Dataset Tag."""

    def __str__(self):
        return (
            f"{self.parent.tree_type}: {self.parent.datagroup} "
            "→ {self.child.datagroup}"
        )


class Tag(node_factory(TagEdge)):
    """A tag for a DataGroup or Dataset."""

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class TagAlias(models.Model):
    """An alternative name for a Tag."""

    name = models.CharField(max_length=100)

    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
