Test repository for the MDE relation model
==========================================

This projects is only used during development to test and discuss the
relation model in the model data explorer.

To install it,

1. run `pip install -r requirements.txt`
2. create a postgres database named `django_dag_test` (see
   [`settings.py`](django_psql_dag_test/settings.py) for the exact settings)
3. run `python manage.py makemigrations`
4. run `python manage.py migrate`
5. experiment with the models via `python manage.py shell`.

UML Class Diagramm
==================

![Models Graph](./models-graph.png)
